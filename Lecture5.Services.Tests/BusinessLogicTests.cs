using System;
using Xunit;
using Lecture4.DataAccess;
using Lecture4.Models.DTOs;
using Lecture4.Models.Models;
using Lecture4.Services;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace Lecture5.Services.Tests
{
    public class BusinessLogicTests
    {
        private readonly IMapper _mapper;

        public BusinessLogicTests()
        {
            _mapper = MapperConfiguration().CreateMapper();
        }

        public MapperConfiguration MapperConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TeamDTO, Team>();
                cfg.CreateMap<Team, TeamDTO>();

                cfg.CreateMap<ProjectDTO, Project>();
                cfg.CreateMap<Project, ProjectDTO>();

                cfg.CreateMap<UserDTO, User>();
                cfg.CreateMap<User, UserDTO>();

                cfg.CreateMap<TaskDTO, Task>();
                cfg.CreateMap<Task, TaskDTO>();
            });

            return config;
        }

        #region Main Tests
        //Main tests 
        //Add user: good case
        //Mark task as finished: good and bad case
        //Get user by id: good and bad case
        //Get project by id: good and bad case
        //Get task by id: good and bad case
        //Get team by id: good and bad case
        //Delete user: good and bad case
        //Delete project: good and bad case
        //Delete task: good and bad case


        [Fact]
        public void WhenAddNewUser_ThenDbContextUsersAddIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeDbSet = A.Fake<DbSet<User>>();
            A.CallTo(() => fakeDbContext.Users).Returns(fakeDbSet);

            UserService service = new UserService(fakeDbContext, _mapper);
            UserDTO userDTO = new UserDTO()
            {
                Id = 1,
                FirstName = "FakeUser"
            };            
            
            //Act
            service.Add(userDTO);

            //Assert
            A.CallTo(() => fakeDbContext.Users.Add(A<User>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.SaveChanges()).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void WhenMarkTaskAsFinished_ThenDbContextTasksFindAndUpdateIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeDbSet = A.Fake<DbSet<Task>>();
            A.CallTo(() => fakeDbContext.Tasks).Returns(fakeDbSet);

            TaskService service = new TaskService(fakeDbContext, _mapper);

            //Act
            service.MarkTaskAsFinished(1);

            //Assert
            A.CallTo(() => fakeDbContext.Tasks.Find(A<int>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.Update(A<Task>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.SaveChanges()).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void WhenMarkTaskAsFinishedWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeDbSet = A.Fake<DbSet<Task>>();
            A.CallTo(() => fakeDbContext.Tasks).Returns(fakeDbSet);

            TaskService service = new TaskService(fakeDbContext, _mapper);

            //Act / Assert
            Assert.Throws<ArgumentException>(() => service.MarkTaskAsFinished(-100));
        }


        [Fact]
        public void WhenGetUserById_ThenDbContextUsersFindIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeUsersSet = A.Fake<DbSet<User>>();

            A.CallTo(() => fakeDbContext.Users).Returns(fakeUsersSet);

            UserService service = new UserService(fakeDbContext, _mapper);

            //Act
            UserDTO userDTO = service.GetById(1);

            //Assert
            A.CallTo(() => fakeDbContext.Users.Find(1)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void WhenGetUserWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeUsersSet = A.Fake<DbSet<User>>();

            A.CallTo(() => fakeDbContext.Users).Returns(fakeUsersSet);

            UserService service = new UserService(fakeDbContext, _mapper);

            //Act / Assert
            Assert.Throws<ArgumentException>(() => service.GetById(-100));
        }

        [Fact]
        public void WhenGetProjectById_ThenDbContextProjectsFindIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeProjectsSet = A.Fake<DbSet<Project>>();

            A.CallTo(() => fakeDbContext.Projects).Returns(fakeProjectsSet);

            ProjectService service = new ProjectService(fakeDbContext, _mapper);

            //Act
            ProjectDTO projectDTO = service.GetById(1);

            //Assert
            A.CallTo(() => fakeDbContext.Projects.Find(1)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void WhenGetProjectWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeProjectsSet = A.Fake<DbSet<Project>>();

            A.CallTo(() => fakeDbContext.Projects).Returns(fakeProjectsSet);

            ProjectService service = new ProjectService(fakeDbContext, _mapper);

            //Act / Assert
            Assert.Throws<ArgumentException>(() => service.GetById(-100));
        }

        [Fact]
        public void WhenGetTaskById_ThenDbContextTasksFindIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeTasksSet = A.Fake<DbSet<Task>>();

            A.CallTo(() => fakeDbContext.Tasks).Returns(fakeTasksSet);

            TaskService service = new TaskService(fakeDbContext, _mapper);

            //Act
            TaskDTO taskDTO = service.GetById(1);

            //Assert
            A.CallTo(() => fakeDbContext.Tasks.Find(1)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void WhenGetTaskWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeTasksSet = A.Fake<DbSet<Task>>();

            A.CallTo(() => fakeDbContext.Tasks).Returns(fakeTasksSet);

            TaskService service = new TaskService(fakeDbContext, _mapper);

            //Act / Assert
            Assert.Throws<ArgumentException>(() => service.GetById(-100));
        }

        [Fact]
        public void WhenGetTeamById_ThenDbContextTeamsFindIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeTeamsSet = A.Fake<DbSet<Team>>();

            A.CallTo(() => fakeDbContext.Teams).Returns(fakeTeamsSet);

            TeamService service = new TeamService(fakeDbContext, _mapper);

            //Act
            TeamDTO teamDTO = service.GetById(1);

            //Assert
            A.CallTo(() => fakeDbContext.Teams.Find(1)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void WhenGetTeamWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeTeamsSet = A.Fake<DbSet<Team>>();

            A.CallTo(() => fakeDbContext.Teams).Returns(fakeTeamsSet);

            TeamService service = new TeamService(fakeDbContext, _mapper);

            //Act / Assert
            Assert.Throws<ArgumentException>(() => service.GetById(-100));
        }

        [Fact]
        public void WhenDeleteUser_ThenDbContextUsersFindAndRemoveIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeUsersSet = A.Fake<DbSet<User>>();

            A.CallTo(() => fakeDbContext.Users).Returns(fakeUsersSet);

            UserService service = new UserService(fakeDbContext, _mapper);

            //Act
            service.Delete(1);

            //Assert
            A.CallTo(() => fakeDbContext.Users.Find(1)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.Users.Remove(A<User>._)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void WhenDeleteUserWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeUsersSet = A.Fake<DbSet<User>>();

            A.CallTo(() => fakeDbContext.Users).Returns(fakeUsersSet);

            UserService service = new UserService(fakeDbContext, _mapper);

            //Act / Assert
            Assert.Throws<ArgumentException>(() => service.Delete(-100));
        }

        [Fact]
        public void WhenDeleteProject_ThenDbContextProjectsFindAndRemoveIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeProjectsSet = A.Fake<DbSet<Project>>();

            A.CallTo(() => fakeDbContext.Projects).Returns(fakeProjectsSet);

            ProjectService service = new ProjectService(fakeDbContext, _mapper);

            //Act
            service.Delete(1);

            //Assert
            A.CallTo(() => fakeDbContext.Projects.Find(1)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.Projects.Remove(A<Project>._)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void WhenDeleteProjectWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeProjectsSet = A.Fake<DbSet<Project>>();

            A.CallTo(() => fakeDbContext.Projects).Returns(fakeProjectsSet);

            ProjectService service = new ProjectService(fakeDbContext, _mapper);

            //Act / Assert
            Assert.Throws<ArgumentException>(() => service.Delete(-100));
        }

        [Fact]
        public void WhenDeleteTask_ThenDbContextTasksFindAndRemoveIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeTasksSet = A.Fake<DbSet<Task>>();

            A.CallTo(() => fakeDbContext.Tasks).Returns(fakeTasksSet);

            TaskService service = new TaskService(fakeDbContext, _mapper);

            //Act
            service.Delete(1);

            //Assert
            A.CallTo(() => fakeDbContext.Tasks.Find(1)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.Tasks.Remove(A<Task>._)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void WhenDeleteTasktWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeTasksSet = A.Fake<DbSet<Task>>();

            A.CallTo(() => fakeDbContext.Tasks).Returns(fakeTasksSet);

            TaskService service = new TaskService(fakeDbContext, _mapper);

            //Act / Assert
            Assert.Throws<ArgumentException>(() => service.Delete(-100));
        }

        #endregion

        #region Another Tests
        [Fact]
        public void WhenDeleteTeam_ThenDbContextTeamsFindAndRemoveIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeTeamsSet = A.Fake<DbSet<Team>>();

            A.CallTo(() => fakeDbContext.Teams).Returns(fakeTeamsSet);

            TeamService service = new TeamService(fakeDbContext, _mapper);

            //Act
            service.Delete(1);

            //Assert
            A.CallTo(() => fakeDbContext.Teams.Find(1)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.Teams.Remove(A<Team>._)).MustHaveHappenedOnceExactly();
        }


        [Fact]
        public void WhenDeleteTeamtWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeTeamsSet = A.Fake<DbSet<Team>>();

            A.CallTo(() => fakeDbContext.Teams).Returns(fakeTeamsSet);

            TeamService service = new TeamService(fakeDbContext, _mapper);

            //Act / Assert
            Assert.Throws<ArgumentException>(() => service.Delete(-100));
        }

        [Fact]
        public void WhenAddNewProject_ThenDbContextProjectsAddIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeDbSet = A.Fake<DbSet<Project>>();
            A.CallTo(() => fakeDbContext.Projects).Returns(fakeDbSet);

            ProjectService service = new ProjectService(fakeDbContext, _mapper);
            ProjectDTO projectDTO = new ProjectDTO()
            {
                Id = 1,
                Name = "FakeProject"
            };

            //Act
            service.Add(projectDTO);

            //Assert
            A.CallTo(() => fakeDbContext.Projects.Add(A<Project>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.SaveChanges()).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void WhenAddNewTask_ThenDbContextTasksAddIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeDbSet = A.Fake<DbSet<Task>>();
            A.CallTo(() => fakeDbContext.Tasks).Returns(fakeDbSet);

            TaskService service = new TaskService(fakeDbContext, _mapper);
            TaskDTO taskDTO = new TaskDTO()
            {
                Id = 1,
                Name = "FakeTask"
            };

            //Act
            service.Add(taskDTO);

            //Assert
            A.CallTo(() => fakeDbContext.Tasks.Add(A<Task>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.SaveChanges()).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void WhenAddNewTeam_ThenDbContextTeamsAddIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeDbSet = A.Fake<DbSet<Team>>();
            A.CallTo(() => fakeDbContext.Teams).Returns(fakeDbSet);

            TeamService service = new TeamService(fakeDbContext, _mapper);
            TeamDTO teamDTO = new TeamDTO()
            {
                Id = 1,
                Name = "FakeTeam"
            };

            //Act
            service.Add(teamDTO);

            //Assert
            A.CallTo(() => fakeDbContext.Teams.Add(A<Team>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeDbContext.SaveChanges()).MustHaveHappenedOnceExactly();
        }
        #endregion

        #region Tests for new Api that returns not finished tasks

        [Fact]
        public void WhenGetNotFinishedTasks_ThenUsersFindIsHappened()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeUsersSet = A.Fake<DbSet<User>>();

            A.CallTo(() => fakeDbContext.Users).Returns(fakeUsersSet);

            LinqService service = new LinqService(fakeDbContext, _mapper);

            //Act
            service.GetNotFinishedTasks(1);

            //Assert
            A.CallTo(() => fakeDbContext.Users.Find(A<int>._)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void WhenGetNotFinishedTasksWithInvalidId_ThenArgumentExceptionIsThrown()
        {
            //Arrange
            var fakeDbContext = A.Fake<MyDbContext>(x => x.WithArgumentsForConstructor(
                () => new MyDbContext(new DbContextOptions<MyDbContext>())));
            var fakeUsersSet = A.Fake<DbSet<User>>();

            A.CallTo(() => fakeDbContext.Users).Returns(fakeUsersSet);

            LinqService service = new LinqService(fakeDbContext, _mapper);

            //Act / Assert
            Assert.Throws<ArgumentException>(() => service.GetNotFinishedTasks(-100));
        }

        #endregion
    }
}
