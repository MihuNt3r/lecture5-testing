﻿using System;
using System.Linq;
using System.Collections.Generic;
using Lecture4.DataAccess;
using Lecture4.Models.Models;
using Lecture4.Models.DTOs;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace Lecture4.Services
{
    public class TeamService
    {
        private readonly MyDbContext _context;
        private readonly IMapper _mapper;

        public TeamService(MyDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IEnumerable<TeamDTO> GetAll()
        {
            var teams = _context.Teams.ToList();

            List<TeamDTO> teamDTOs = _mapper.Map<List<TeamDTO>>(teams);

            return teamDTOs;
        }

        public TeamDTO GetById(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var team = _context.Teams.Find(id);

            if (team == null)
                throw new Exception("Can't find team with such id");

            TeamDTO teamDTO = _mapper.Map<TeamDTO>(team);

            return teamDTO;
        }

        public void Add(TeamDTO teamDTO)
        {
            if (teamDTO.Id < 0)
                throw new ArgumentException("Wrong id");

            var team = _mapper.Map<Team>(teamDTO);

            _context.Teams.Add(team);
            _context.SaveChanges();
        }

        public void Update(TeamDTO teamDTO)
        {
            var team = _mapper.Map<Team>(teamDTO);

            _context.Update(team);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var team = _context.Teams.Find(id);

            if (team == null)
                throw new Exception("Can't find team with such id");

            _context.Teams.Remove(team);
            _context.SaveChanges();
        }
    }
}
