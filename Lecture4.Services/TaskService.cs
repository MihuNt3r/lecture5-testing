﻿using System;
using System.Linq;
using System.Collections.Generic;
using Lecture4.DataAccess;
using Lecture4.Models.Models;
using Lecture4.Models.DTOs;
using AutoMapper;

namespace Lecture4.Services
{
    public class TaskService
    {
        private readonly MyDbContext _context;
        private readonly IMapper _mapper;

        public TaskService(MyDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IEnumerable<TaskDTO> GetAll()
        {
            var tasks = _context.Tasks.ToList();

            List<TaskDTO> taskDTOs = _mapper.Map<List<TaskDTO>>(tasks);

            return taskDTOs;
        }

        public TaskDTO GetById(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var task = _context.Tasks.Find(id);

            if (task == null)
                throw new Exception("Can't find task with such id");

            TaskDTO taskDTO = _mapper.Map<TaskDTO>(task);

            return taskDTO;
        }

        public void MarkTaskAsFinished(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var task = _context.Tasks.Find(id);

            if (task == null)
                throw new Exception("Can't find task with such id");

            if (task.FinishedAt != null)
                throw new ArgumentException("Task is already finished");

            task.FinishedAt = DateTime.Now;

            _context.Update(task);
            _context.SaveChanges();
        }

        public void Add(TaskDTO taskDTO)
        {
            if (taskDTO.Id < 0)
                throw new ArgumentException("Wrong id");

            var task = _mapper.Map<Task>(taskDTO);

            _context.Tasks.Add(task);
            _context.SaveChanges();
        }

        public void Update(TaskDTO taskDTO)
        {
            var task = _mapper.Map<Task>(taskDTO);

            _context.Update(task);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var task = _context.Tasks.Find(id);

            if (task == null)
                throw new Exception("Can't find task with such id");

            _context.Tasks.Remove(task);
            _context.SaveChanges();
        }


    }
}
