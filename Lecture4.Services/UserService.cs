﻿using System;
using System.Linq;
using System.Collections.Generic;
using Lecture4.DataAccess;
using Lecture4.Models.Models;
using Lecture4.Models.DTOs;
using AutoMapper;

namespace Lecture4.Services
{
    public class UserService
    {
        private readonly MyDbContext _context;
        private readonly IMapper _mapper;

        public UserService(MyDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IEnumerable<UserDTO> GetAll()
        {
            var users = _context.Users.ToList();

            List<UserDTO> userDTOs = _mapper.Map<List<UserDTO>>(users);

            return userDTOs;
        }

        public UserDTO GetById(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var user = _context.Users.Find(id);

            if (user == null)
                throw new Exception("Can't find user with such id");

            UserDTO userDTO = _mapper.Map<UserDTO>(user);

            return userDTO;
        }

        public void AddUserToTeam(int userId, int teamId)
        {
            var user = _context.Users.Find(userId);

            if (user == null)
                throw new Exception("Can't find user with such id");

            var team = _context.Teams.Find(teamId);

            if (team == null)
                throw new Exception("Can't find team with such id");

            user.Team = team;
            _context.Update(user);
            _context.SaveChanges();
        }

        public void Add(UserDTO userDTO)
        {
            if (userDTO.Id < 0)
                throw new ArgumentException("Wrong id");

            var user = _mapper.Map<User>(userDTO);

            _context.Users.Add(user);
            _context.SaveChanges();
        }

        public void Update(UserDTO userDTO)
        {
            var user = _mapper.Map<User>(userDTO);

            _context.Update(user);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var user = _context.Users.Find(id);

            if (user == null)
                throw new Exception("Can't find user with such id");

            _context.Users.Remove(user);
            _context.SaveChanges();
        }
    }
}
