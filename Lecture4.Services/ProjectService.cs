﻿using System;
using System.Linq;
using System.Collections.Generic;
using Lecture4.DataAccess;
using Lecture4.Models.Models;
using Lecture4.Models.DTOs;
using AutoMapper;

namespace Lecture4.Services
{
    public class ProjectService
    {
        private readonly MyDbContext _context;
        private readonly IMapper _mapper;

        public ProjectService(MyDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IEnumerable<ProjectDTO> GetAll()
        {
            var projects = _context.Projects.ToList();

            List<ProjectDTO> projectDTOs = _mapper.Map<List<ProjectDTO>>(projects);

            return projectDTOs;
        }

        public ProjectDTO GetById(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var project = _context.Projects.Find(id);

            if (project == null)
                throw new Exception("Can't find project with such id");

            ProjectDTO projectDTO = _mapper.Map<ProjectDTO>(project);

            return projectDTO;
        }

        public void Add(ProjectDTO projectDTO)
        {
            if (projectDTO.Id < 0)
                throw new ArgumentException("Wrong id");

            var project = _mapper.Map<Project>(projectDTO);

            _context.Projects.Add(project);
            _context.SaveChanges();
        }

        public void Update(ProjectDTO projectDTO)
        {
            var project = _mapper.Map<Project>(projectDTO);

            _context.Update(project);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var project = _context.Projects.Find(id);

            if (project == null)
                throw new Exception("Can't find project with such id");

            _context.Projects.Remove(project);
            _context.SaveChanges();
        }
    }
}
