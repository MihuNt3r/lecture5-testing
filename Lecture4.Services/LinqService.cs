﻿using System;
using System.Linq;
using System.Collections.Generic;
using Lecture4.DataAccess;
using Lecture4.Models.Models;
using Lecture4.Models.AuxiliaryModels;
using Microsoft.EntityFrameworkCore;
using Lecture4.Models.DTOs;
using AutoMapper;

namespace Lecture4.Services
{
    public class LinqService
    {
        private readonly MyDbContext _context;
        private readonly IMapper _mapper;
        
        public LinqService(MyDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public Dictionary<ProjectDTO, int> GetDictionary(int id)
        {
            var tasks = _context.Tasks
                .Include(t => t.Project).ToList();

            var query = (from t in tasks
                         where t.PerformerId == id
                         select new
                         {
                             Proj = _mapper.Map<ProjectDTO>(t.Project),
                             TasksCount = t.Project.Tasks.Count
                         }).Distinct().ToDictionary(t => t.Proj, t => t.TasksCount);

            return query;
        }

        public List<TaskDTO> GetTasksForUserById(int id)
        {
            var tasks = _context.Tasks.ToList();

            var query = (from t in tasks
                         where t.PerformerId == id && t.Name.Length < 45
                         select _mapper.Map<TaskDTO>(t)).ToList();

            return query;
        }

        public List<TaskIdAndName> GetTasksFinishedIn2021ByUser(int id)
        {
            var tasks = _context.Tasks.ToList();

            var query = (from t in tasks
                         where t.PerformerId == id
                         where t.FinishedAt?.Year == 2021
                         select new TaskIdAndName
                         {
                             Id = t.Id,
                             Name = t.Name
                         }).ToList();

            return query;
        }

        public List<TaskDTO> GetNotFinishedTasks(int id)
        {
            if (id < 0)
                throw new ArgumentException("Wrong id");

            var users = _context.Users
                .Include(u => u.Tasks);

            var user = users.FirstOrDefault(u => u.Id == id);

            if (user == null)
                throw new Exception("Can't find user with such id");

            var notFinishedTasks = user.Tasks.Where(t => t.FinishedAt == null);

            var taskDTOs = _mapper.Map<List<TaskDTO>>(notFinishedTasks);

            return taskDTOs;
        }

        public List<TeamIdNameAndUsers> GetListOfTasksWithUsersOlderThan10()
        {
            var teams = _context.Teams
                .Include(t => t.Customers).ToList();

            var query = (from t in teams
                         where t.Customers.All(c => c.BirthDay.Year < DateTime.Now.Year - 10)
                         select new TeamIdNameAndUsers
                         {
                             Id = t.Id,
                             Name = t.Name,
                             Users = (from u in t.Customers
                                      orderby u.RegisteredAt descending
                                      select _mapper.Map<UserDTO>(u)).ToList()
                         }).Distinct().ToList();

            return query;
        }

        public List<UserNameAndSortedTasks> GetSortedUsersWithSortedTasks()
        {
            var users = _context.Users
                .Include(u => u.Tasks).ToList();

            var query = (from u in users
                         orderby u.FirstName
                         select new UserNameAndSortedTasks
                         {
                             FirstName = u.FirstName,
                             Tasks = (from t in u.Tasks
                                      orderby t.Name.Length descending
                                      select _mapper.Map<TaskDTO>(t)).ToList()
                         }).ToList();

            return query;
        }

        public UserAndInfoAboutHisTasksAndProjects GetUserAndInfoAboutHisTasksAndProjects(int id)
        {
            var tasks = _context.Tasks
                .Include(t => t.Project).ToList();

            var users = _context.Users
                .Include(u => u.Tasks).ToList();
            
            var query = (from u in users
                         where u.Id == id
                         select new UserAndInfoAboutHisTasksAndProjects
                         {
                             User = _mapper.Map<UserDTO>(u),
                             LastProjectName = u.Tasks.Aggregate((curLast, x) => curLast.CreatedAt < x.CreatedAt ? x : curLast).Project.Name,
                             LastProjectTasks = u.Tasks.Aggregate((curLast, x) => curLast.CreatedAt < x.CreatedAt ? x : curLast).Project.Tasks.Count,
                             NotFinishedTasksCount = (from t in u.Tasks
                                                     where t.FinishedAt == null
                                                     select t).ToList().Count,
                             TaskWithLongestPeriodName = u.Tasks.Aggregate((curLongst, x) => 
                            (((curLongst.FinishedAt ?? DateTime.Now) - curLongst.CreatedAt) < ((x.FinishedAt ?? DateTime.Now) - x.CreatedAt)) ? 
                            x : curLongst).Name
                         }).FirstOrDefault();

            return query;
        }

        public List<ProjectAndInfoAboutItsTasksAndCustomers> GetProjectAndInfoAboutItsTasksAndCustomers()
        {
            var projects = _context.Projects
                .Include(p => p.Team.Customers)
                .Include(p => p.Tasks).ToList();

            var query = (from p in projects
                         select new ProjectAndInfoAboutItsTasksAndCustomers
                         {
                             Project = _mapper.Map<ProjectDTO>(p),
                             LongestTaskByDescription = p.Tasks.Count > 0 ?
                             p.Tasks.Aggregate((curMax, x) => curMax.Description.Length < x.Description.Length ? x : curMax).Name : null,

                             ShortestTaskByName = p.Tasks.Count > 0 ?
                             p.Tasks.Aggregate((curMin, x) => curMin.Name.Length > x.Name.Length ? x : curMin).Name : null,

                             CustomersCount = p.Description.Length > 20 || p.Tasks.Count < 3 ?
                             p.Team.Customers.Count : 0
                         }).ToList();

            return query;
        }
    }
}
