﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Lecture4.Models.AuxiliaryModels;
using Lecture4.Models.DTOs;
using Lecture4.Services;
using System;

namespace Lecture4.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _service;

        public LinqController(LinqService service)
        {
            _service = service;
        }

        [HttpGet("getdictionary{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<Dictionary<ProjectDTO, int>> GetDictionary(int id)
        {
            return _service.GetDictionary(id);
        }

        [HttpGet("gettasksforuser/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<List<TaskDTO>> GetListOfTasks(int id)
        {
            return _service.GetTasksForUserById(id);
        }

        [HttpGet("gettasksfinishedin2021byuser/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<List<TaskIdAndName>> GetFinishedTasks(int id)
        {
            return _service.GetTasksFinishedIn2021ByUser(id);
        }

        [HttpGet("getnotfinishedtasksforuser/{id}")]
        public ActionResult<List<TaskDTO>> GetNotFinishedTasks(int id)
        {
            try
            {
                return _service.GetNotFinishedTasks(id);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("getteamswhereallusersolderthan10")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<List<TeamIdNameAndUsers>> GetTeamsWithUsersOlderThan10()
        {
            return _service.GetListOfTasksWithUsersOlderThan10();
        }

        [HttpGet("getsorteduserswithsortedtasks")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<List<UserNameAndSortedTasks>> GetSortedUsersAndSortedTasks()
        {
            return _service.GetSortedUsersWithSortedTasks();
        }

        [HttpGet("getuserandinfoabouthistasksandprojects/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<UserAndInfoAboutHisTasksAndProjects> GetUserAndInfoAboutHisTasksAndProjects(int id)
        {
            return _service.GetUserAndInfoAboutHisTasksAndProjects(id);
        }

        [HttpGet("getprojectandinfoaboutitstasksandcustomers")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<List<ProjectAndInfoAboutItsTasksAndCustomers>> GetProjectsInfo()
        {
            return _service.GetProjectAndInfoAboutItsTasksAndCustomers();
        }
    }
}
