﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using Lecture4.Models.DTOs;
using Lecture4.Services;
using Microsoft.EntityFrameworkCore;

namespace Lecture4.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly TaskService _service;

        public TaskController(TaskService service)
        {
            _service = service;
        }

        // GET: api/Task
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<TaskDTO>> GetTeams()
        {
            var taskDTOs = _service.GetAll();

            return Ok(taskDTOs);
        }

        // GET: api/Task/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<TaskDTO> GetTeam(int id)
        {
            TaskDTO taskDTO;
            try
            {
                taskDTO = _service.GetById(id);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

            return Ok(taskDTO);
        }

        // PUT: api/Task/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult Put([FromBody] TaskDTO taskDTO)
        {
            if (taskDTO == null)
                return BadRequest();

            try
            {
                _service.Update(taskDTO);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
            return Ok();
        }

        // POST: api/Task
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult Post([FromBody] TaskDTO taskDTO)
        {
            if (taskDTO == null)
                return BadRequest();

            if (taskDTO.Id < 0)
                return BadRequest("Wrong request body");

            try
            {
                _service.Add(taskDTO);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return Created("", taskDTO);
        }

        // DELETE: api/Task/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult Delete(int id)
        {
            try
            {
                _service.Delete(id);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (DbUpdateException)
            {
                return Conflict("Cascade deleting is not supported");
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }


            return NoContent();
        }
    }
}
