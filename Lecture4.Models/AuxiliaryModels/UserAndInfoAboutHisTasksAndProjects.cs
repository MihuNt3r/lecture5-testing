﻿using Lecture4.Models.Models;
using Lecture4.Models.DTOs;

namespace Lecture4.Models.AuxiliaryModels
{
    public class UserAndInfoAboutHisTasksAndProjects
    {
        public UserDTO User { get; set; }
        public string LastProjectName { get; set; }
        public int LastProjectTasks { get; set; }
        public int NotFinishedTasksCount { get; set; }
        public string TaskWithLongestPeriodName { get; set; }
    }
}
