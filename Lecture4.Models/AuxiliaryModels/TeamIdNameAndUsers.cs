﻿using System.Collections.Generic;
using Lecture4.Models.DTOs;

namespace Lecture4.Models.AuxiliaryModels
{
    public class TeamIdNameAndUsers
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserDTO> Users { get; set; }
    }
}
