﻿using System.Collections.Generic;
using Lecture4.Models.DTOs;

namespace Lecture4.Models.AuxiliaryModels
{
    public class UserNameAndSortedTasks
    {
        public string FirstName { get; set; }
        public List<TaskDTO> Tasks { get; set; }
    }
}
