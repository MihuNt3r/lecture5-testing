﻿using Lecture4.Models.DTOs;

namespace Lecture4.Models.AuxiliaryModels
{
    public class ProjectAndInfoAboutItsTasksAndCustomers
    {
        public ProjectDTO Project { get; set; }
        public string LongestTaskByDescription { get; set; }
        public string ShortestTaskByName { get; set; }
        public int CustomersCount { get; set; }
    }
}
