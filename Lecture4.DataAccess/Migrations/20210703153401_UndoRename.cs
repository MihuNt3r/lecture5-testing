﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Lecture4.DataAccess.Migrations
{
    public partial class UndoRename : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RegistrationDate",
                table: "Users",
                newName: "RegisteredAt");

            migrationBuilder.RenameColumn(
                name: "CreationDate",
                table: "Teams",
                newName: "CreatedAt");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RegisteredAt",
                table: "Users",
                newName: "RegistrationDate");

            migrationBuilder.RenameColumn(
                name: "CreatedAt",
                table: "Teams",
                newName: "CreationDate");
        }
    }
}
