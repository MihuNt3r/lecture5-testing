﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using Xunit;
using Lecture4.WebApi;
using Lecture4.Models.DTOs;
using Microsoft.AspNetCore.Mvc.Testing;

namespace Lecture5.WebApi.IntegrationTests
{
    public class WebApiIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public WebApiIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }
        
        #region Main Tests
        //Main Tests
        //Add project: good and bad case
        //Delete user: good and bad case
        //Add team: good and bad case
        //Get team by id: good and bad case
        //Delete task: good and bad case

            [Fact]
            public async System.Threading.Tasks.Task WhenAddProject_ThenResponseWithCode201AndProjectsCountInDataBaseIsEqualTo1()
            {
                //Arrange
                var project = new ProjectDTO()
                { 
                    Id = 0,
                    AuthorId = 1,
                    Deadline = DateTime.Now,
                    Description = "Test Description",
                    Name = "Test Name",
                    TeamId = 1                
                };

                string jsonString = JsonConvert.SerializeObject(project);

                //Act
                var httpPost = await _client.PostAsync("api/projects", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var httpResponse = await _client.GetAsync("api/projects");
                var stringResponse = await httpResponse.Content.ReadAsStringAsync();
                var projects = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);

                //Assert
                int expectedProjectsCount = 1;
                Assert.Equal(HttpStatusCode.Created, httpPost.StatusCode);
                Assert.Equal(expectedProjectsCount, projects.Count);
            }

            [Fact]
            public async System.Threading.Tasks.Task WhenAddProjectWithNegativeId_ThenResponseCode400AndEmptyDb()
            {
                //Arrange
                var project = new ProjectDTO()
                { 
                    Id = -999,
                    AuthorId = 2,
                    Deadline = DateTime.Now,
                    Description = "Test Description",
                    Name = "Test Name",
                    TeamId = 2                
                };

                string jsonString = JsonConvert.SerializeObject(project);

                //Act
                var httpPost = await _client.PostAsync("api/projects", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var httpResponse = await _client.GetAsync("api/projects");
                var stringResponse = await httpResponse.Content.ReadAsStringAsync();
                var projects = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);

                //Assert
                int expectedProjectsCount = 0;
                Assert.Equal(HttpStatusCode.BadRequest, httpPost.StatusCode);
                Assert.Equal(expectedProjectsCount, projects.Count);
            }

            [Fact]
            public async System.Threading.Tasks.Task WhenDeleteExistingUser_ThenResponseCode204AndDbContextUsersIsEmpty()
            {
                //Arrange
                var user = new UserDTO()
                {
                    Id = 0,
                    BirthDay = DateTime.Parse("14.02.1977"),
                    Email = "Felizia.Kirilin@yahoo.com",
                    FirstName = "Felizia",
                    LastName = "Kirilin",
                    RegisteredAt = DateTime.Parse("23.08.2020"),
                    TeamId = 4
                };

                string jsonString = JsonConvert.SerializeObject(user);

                //Act
                var httpPost = await _client.PostAsync("api/user", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                var httpDelete = await _client.DeleteAsync("api/user/1");

                var httpResponse = await _client.GetAsync("api/user");
                var stringResponse = await httpResponse.Content.ReadAsStringAsync();
                var users = JsonConvert.DeserializeObject<List<UserDTO>>(stringResponse);

                //Assert
                int expectedUsersCount = 0;
                Assert.Equal(HttpStatusCode.NoContent, httpDelete.StatusCode);
                Assert.Equal(expectedUsersCount, users.Count);
            }

            [Fact]
            public async System.Threading.Tasks.Task WhenDeleteNotExistingUser_ThenResponseCode404()
            {
                //Arrange / Act
                var httpDelete = await _client.DeleteAsync("api/user/99");

                //Assert
                Assert.Equal(HttpStatusCode.NotFound, httpDelete.StatusCode);
            }

            [Fact]
            public async System.Threading.Tasks.Task WhenAddTeamWithNegativeId_ThenResponseWithCode400()
            {
                //Arrange
                var team = new TeamDTO()
                { 
                    Id = -999,
                    CreatedAt = DateTime.Now,
                    Name = "TestTeam88"
                };

                string jsonString = JsonConvert.SerializeObject(team);

                //Act
                var httpPost = await _client.PostAsync("api/team", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var httpResponse = await _client.GetAsync("api/team");
                var stringResponse = await httpResponse.Content.ReadAsStringAsync();
                var teams = JsonConvert.DeserializeObject<List<TeamDTO>>(stringResponse);

                //Assert
                Assert.Equal(HttpStatusCode.BadRequest, httpPost.StatusCode);
            }

            [Fact]
            public async System.Threading.Tasks.Task WhenAddTeam_ThenResponseWithCode201AndTeamsCountInDataBaseIsEqualTo1()
            {
                //Arrange
                var team = new TeamDTO()
                { 
                    Id = 0,
                    CreatedAt = DateTime.Now,
                    Name = "TestTeam"
                };

                string jsonString = JsonConvert.SerializeObject(team);

                //Act
                var httpPost = await _client.PostAsync("api/team", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var httpResponse = await _client.GetAsync("api/team");
                var stringResponse = await httpResponse.Content.ReadAsStringAsync();
                var teams = JsonConvert.DeserializeObject<List<TeamDTO>>(stringResponse);

                //Assert
                int expectedTeamsCount = 1;
                Assert.Equal(HttpStatusCode.Created, httpPost.StatusCode);
                Assert.Equal(expectedTeamsCount, teams.Count);
            }

            [Fact]
            public async System.Threading.Tasks.Task GetTeamById_WhenTeamNotExist_ThenResponseCode404()
            {
                //Arrange / Assert
                var httpResponse = await _client.GetAsync("api/team/999");

                //Assert
                Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
            }

            [Fact]
            public async System.Threading.Tasks.Task GetTeamById_WhenTeamExist_ThenResponseCode200()
            {
                //Arrange
                var team = new TeamDTO()
                { 
                    Id = 0,
                    CreatedAt = DateTime.Now,
                    Name = "TestTeam"
                };

                string jsonString = JsonConvert.SerializeObject(team);

                //Act
                var httpPost = await _client.PostAsync("api/team", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var httpResponse = await _client.GetAsync("api/team/1");
                var stringResponse = await httpResponse.Content.ReadAsStringAsync();

                //Assert
                Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            }

            [Fact]
            public async System.Threading.Tasks.Task DeleteTask_WhenTaskExists_ThenResponseCode204AndEmptyDb()
            {
                //Arrange
                var task = new TaskDTO()
                { 
                    Id = 0,
                    Name = "TestTask",
                    CreatedAt = DateTime.Now,
                    Description = "TestTaskDescription",
                    PerformerId = 1,
                    ProjectId = 1,
                    State = 1,
                };

                string jsonString = JsonConvert.SerializeObject(task);

                //Act
                var httpPost = await _client.PostAsync("api/task", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                var httpDelete = await _client.DeleteAsync("api/task/1");

                var httpResponse = await _client.GetAsync("api/task");
                var stringResponse = await httpResponse.Content.ReadAsStringAsync();
                var tasks = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);

                //Assert
                int expectedTasksCount = 0;
                Assert.Equal(HttpStatusCode.NoContent, httpDelete.StatusCode);
                Assert.Equal(expectedTasksCount, tasks.Count);
            }

            [Fact]
            public async System.Threading.Tasks.Task DeleteTask_WhenNoPosts_ThenResponseCode400AndEmptyDb()
            {
                //Arrange / Act
                var httpDelete = await _client.DeleteAsync("api/task/999");

                var httpResponse = await _client.GetAsync("api/task");
                var stringResponse = await httpResponse.Content.ReadAsStringAsync();
                var tasks = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);

                //Assert
                int expectedTasksCount = 0;
                Assert.Equal(HttpStatusCode.NotFound, httpDelete.StatusCode);
                Assert.Equal(expectedTasksCount, tasks.Count);
            }
            #endregion

            #region Tests for New Api that returns not finished tasks

            [Fact]
            public async System.Threading.Tasks.Task GetUnfinishedTasks_WhenUserHasNoTasks_ThenResponseCode200AndUnfinishedTasksCountIs0()
            {
                //Arrange
                UserDTO user = new UserDTO
                {
                    Id = 7,
                    BirthDay = DateTime.Parse("14.02.1977"),
                    Email = "Felizia.Kirilin@yahoo.com",
                    FirstName = "Felizia",
                    LastName = "Kirilin",
                    RegisteredAt = DateTime.Parse("23.08.2020"),
                    TeamId = 4
                };

                string jsonString = JsonConvert.SerializeObject(user);
                var httpPost = await _client.PostAsync("api/user", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                //Act
                var httpResponse = await _client.GetAsync("api/linq/getnotfinishedtasksforuser/7");
                var stringResponse = await httpResponse.Content.ReadAsStringAsync();
                var tasks = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);

                //Assert
                int expectedTasksCount = 0;
                Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
                Assert.Equal(expectedTasksCount, tasks.Count);
            }

            [Fact]
            public async System.Threading.Tasks.Task GetUnfinishedTasks_WhenUserNotExists_ThenResponseCode404()
            {
                //Arrange / Act
                var httpRequest = await _client.GetAsync("api/linq/getnotfinishedtasksforuser/999");

                //Assert
                Assert.Equal(HttpStatusCode.NotFound, httpRequest.StatusCode);
            }

        #endregion
    }
}
